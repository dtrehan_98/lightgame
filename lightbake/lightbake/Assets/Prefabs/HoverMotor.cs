﻿using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;

public class HoverMotor : MonoBehaviour
{

    public float speed = 90f;
    public float turnSpeed = 5f;
    public float hoverForce = 65f;
    public float hoverHeight = 3.5f;
    private float powerInput;
    private float turnInput;
    private Rigidbody carRigidbody;

    // input values
    public Vector2 moveInput = Vector2.zero;


    void Awake()
    {
        carRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // old input
        //powerInput = Input.GetAxis("Vertical");
        //turnInput = Input.GetAxis("Horizontal");

        // new Input
        powerInput = moveInput.y;
        turnInput = moveInput.x;                                                                                       
    }

    void FixedUpdate()
    {
        Ray ray = new Ray(transform.position, -transform.up);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, hoverHeight))
        {
            float proportionalHeight = (hoverHeight - hit.distance) / hoverHeight;
            Vector3 appliedHoverForce = Vector3.up * proportionalHeight * hoverForce;
            carRigidbody.AddForce(appliedHoverForce, ForceMode.Acceleration);
        }

        carRigidbody.AddRelativeForce(0f, 0f, powerInput * speed);
        carRigidbody.AddRelativeTorque(0f, turnInput * turnSpeed, 0f);

    }


    // hook for new input system
    public void OnMove(InputAction.CallbackContext context)
    {
        Debug.Log("onMove");
        moveInput = context.ReadValue<Vector2>();
        //moveInput = input.Get<Vector2>();
    }
}